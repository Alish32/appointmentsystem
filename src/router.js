import Vue from 'vue'
import Router from 'vue-router'
import PurchaseOrders from './views/PurchaseOrders.vue'
import SignIn from './views/SignIn.vue'
import SignUp from './views/SignUp.vue'
import PasswordVerification from './views/PasswordVerification.vue'
import Calendar from './views/Calendar.vue'
import PrivateService from './views/PrivateService.vue'
import CustomerCalendar from './views/CustomerCalendar.vue'
import AddService from './views/AddService.vue'
import ChooseService from './views/ChooseService.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: PurchaseOrders
    },
    {
      path: '/daxil-ol',
      name: 'signin',
      component: SignIn
    },
    {
      path: '/qeydiyyat',
      name: 'signup',
      component: SignUp
    },
    {
      path: '/verification',
      name: 'verification',
      component: PasswordVerification
    },
    {
      path: '/calendar',
      name: 'calendar',
      component: Calendar
    },
    {
      path: '/customer-calendar',
      name: 'CustomerCalendar',
      component: CustomerCalendar
    },
    {
      path: '/privateservice',
      name: 'privateservice',
      component: PrivateService
    },
    {
      path: '/addservice',
      name: 'addservice',
      component: AddService
    },
    {
      path: '/chooseservice/:id',
      name: 'chooseservice',
      component: ChooseService
    },
  ]
})
