import Vue from 'vue'
import App from './App.vue'


import router from './router'
import Vuetify from 'vuetify'

import './plugins/vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueCookies from 'vue-cookies'
Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(VueCookies)


new Vue({
  router,
  render: function (h) { return h(App) }
}).$mount('#app')
